import os.path
import re
import platform
from subprocess import PIPE, Popen


class Wifi:


	@classmethod
	def scan(cls):

		if platform.system() == "Darwin":

			cmd = "/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport"

			if os.path.exists(cmd):

				cmd += " -s"
		
				process = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE).communicate()[0].decode()
				result = []

				for v in process.split("\n")[1:-1]:

					data = {}
					count = 0
					switch = True

					for k in v.split(" "):

						if k not in (" "):

							if re.search(r"^([0-9a-fA-F]{2}:){5}?([0-9a-fA-F]{2})$", k) is not None:

								data["BSSID"] = k
								switch = False
							else:
								if switch:
									if count == 0:

										data["SSID"] = k
										count = 1

									else:
										data["SSID"] += (" " + k)
								else:
									count += 1 

									if count == 2:
										data["RSSI"] = int(k)


									if count == 3:
										data["CHANNEL"] = int(k.split(",")[0])


									if count == 4:
										data["HT"] = k


									if count == 5:
										data["CC"] = k


									if count == 6:
										data["SECURITY"] = [k]


									if count > 6:
										data["SECURITY"].append(k)


					data["FREQUENCY"] = Wifi.channel2Frequency(data["CHANNEL"])			
					result.append(data)

				return result



	@classmethod
	def channel2Frequency(cls, channel):

		if isinstance(channel, int):

			if channel > 0:

				frequency = {

					# 2.4 GHz

					1: "2.412",
					2: "2.417",
					3: "2.422",
					4: "2.427",
					5: "2.432",
					6: "2.437",
					7: "2.442",
					8: "2.447",
					9: "2.452",
					10: "2.457",
					11: "2.462",
					12: "2.467",
					13: "2.472",
					14: "2.484",

					# 5 GHz

					36: "5.180",
					40: "5.200",
					44: "5.220",
					48: "5.240",
					52: "5.260",
					56: "5.280",
					60: "5.300",
					64: "5.320",
					100: "5.500",
					104: "5.520",
					108: "5.540",
					112: "5.560",
					116: "5.580",
					120: "5.600",
					124: "5.620",
					128: "5.640",
					132: "5.660",
					136: "5.680",
					140: "5.700",
					144: "5.720",
					149: "5.746",
					153: "5.765",
					157: "5.785",
					161: "5.805",
					165: "5.825",
				}


				if channel in frequency.keys():
					return frequency[channel]