from Wifi import Wifi
from time import time

start = time()
wifi = Wifi.scan()

if isinstance(wifi, list):

	for data in wifi:

		if isinstance(data, dict):
			for key, value in data.items():
				print(key, ":", value)
				
			print("\n", "-"*50, "\n")


print("Time :", round((time() - start), 2), "seconds\n")


"""
for i in range(1, 500):

	data = Wifi.channel2Frequency(i)

	if data:
		print(i, ":", data)
"""